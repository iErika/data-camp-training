import matplotlib.pyplot as plt
import pandas as pd

# Raw data
iris_data_source = ('https://archive.ics.uci.edu/ml/'
                    'machine-learning-databases/iris/iris.data')
col_names = [
    'sepal_length',
    'sepal_width',
    'petal_length',
    'petal_width',
    'species',
]
df = pd.read_csv(iris_data_source, names=col_names)

# Processed data
proc_df = pd.DataFrame()

# Plot
df.plot(y='sepal_width', x='sepal_length', kind='scatter')
plt.show()

df.plot(y='sepal_length', kind='box')
plt.show()